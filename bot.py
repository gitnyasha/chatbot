import telebot
import time

TOKEN = '872793591:AAG5KCPdcv8MkmJjgvXqnERtR5b978f-j7w'

bot = telebot.TeleBot(TOKEN)


@bot.message_handler(content_types=['text'])
def text_to_me(message):
    text = message.text.lower()
    chat_id = message.chat.id
    # Greetings
    if text == "hesi" or text == "hello":
        bot.send_message(chat_id, "How are you!".upper())
    # thanking
    if "thank" in text or "thanx" in text:
        bot.send_message(chat_id, "You are welcome".upper())
    # phone queries
    if "phone" in text or "galaxy" in text or "huawei" in text or "foni" in text:
        bot.send_message(chat_id,
                         "How may I help you with your phone? Type...\n\nEmail?\nSettings?\nApps?\n"
                         "Freezes/slow?\nTo put music on iphone type 'ITUNES'?\nbattery draining?")
    if "email" in text:
        bot.send_message(chat_id,
                         "Do you want to send an email?\nAdd an account?\nremove account?\ndelete emails?\nStop "
                         "receiving emails you don't like?")
    if "send" in text:
        bot.send_message(chat_id,
                         "On your phone or tablet, open the Gmail app .\nIn the bottom right, tap Compose or a circle "
                         "with a plus sign .\nIn the \"To\" field, add recipients(. If you want, you can also add "
                         "recipients in the \"Cc\" and \"Bcc\" fields.\nAdd a subject.\nWrite your message.\nAt the "
                         "top of the page, tap Send")
    if "add" in text:
        bot.send_message(chat_id,
                         "Go to the gmail app and tap on the account icon top right")
    if "stop" in text:
        bot.send_message(chat_id,
                         "Go to the bottom of the email you will see where it says unsubscribe")
    if "remove" in text:
        bot.send_message(chat_id,
                         "Go to the gmail app and tap on the account icon top right and click on manage accounts on "
                         "this device")
    if "delete" in text:
        bot.send_message(chat_id,
                         "do a long press on the email until an option pop up to delete which can be a sign of a bin")
    if "setting" in text:
        bot.send_message(chat_id,
                         "Do you want to config mobile internet settings?\n" +
                         "Add wifi network?\n" +
                         "How to use bluetooth?\n" +
                         "Mobile hotspot\n" +
                         "Vpn\n")
    if "wifi" in text:
        bot.send_message(chat_id,
                         "tap add new network or other if you use an iphone, enter the network" +
                         "name and the wifi security type its usually WPA2")
    if "bluetooth" in text:
        bot.send_message(chat_id,
                         "switch on bluetooth and search for nearby devices and pair then go to" +
                         "the file you want to send and select send with bluetooth")
    if "hotspot" in text:
        bot.send_message(chat_id,
                         "Hit the More button at the bottom of the Wireless & networks section, right below Data "
                         "usage.\n" +
                         "Open Tethering and portable hotspot.\n" +
                         "Tap on Set up Wi-Fi hotspot.\n" +
                         "Input a Network name. ...\n" +
                         "Choose a Security type")
    if "vpn" in text:
        bot.send_message(chat_id,
                         "download any vpn app on app store or play store then it will " +
                         "automatically integrate vpn settings")
    if "internet" in text:
        bot.send_message(chat_id,
                         "Tap Settings.\n" +
                         "Tap Mobile networks.\n" +
                         "Tap Access Point Names.\n" +
                         "Tap the Menu button.\n" +
                         "Tap New APN.\n" +
                         "Tap the Name field. e.g econet, netone, telecel\n" +
                         "Enter the apn - econet.net, net.netone, telecel.net then tap OK\n" +
                         "iphone users Settings > Mobile Data > Mobile Data Options > Mobile Data Network.\n" +
                         "apn - econet.net, net.netone, telecel.net")
    if "app" in text:
        bot.send_message(chat_id,
                         "you can download apps on app store or play store first you need to have" +
                         " an icloud account if you use iphone and you can setup icloud in settings\n" +
                         "if you use android you need to have an email account synced with the phone you can find" +
                         "it settings/accounts and add account")
    if "freeze" in text or "slow" in text:
        bot.send_message(chat_id,
                         "If you have a ton of apps that is causing a performance dip on your Android" +
                         "device, you have the option to disable or freeze the app.\n" +
                         "Some devices suffer from a low-memory issue and most perform poorly once they hit" +
                         "around 80% of their device storage capacity. If your device are showing signs of lag, " +
                         "it may be time to do some spring cleaning. Go to settings/storage and tap clean cache")
    if "create icloud" in text:
        bot.send_message(chat_id,
                         "Go to settings ICloud.\n Enter your email address and tap forgot password or id\n" +
                         "in case you forgot just the password enter your apple id and click next\n" +
                         "In case you got forgot both tap forgot ID and enter your email address and name inorder to "
                         "receive the Apple ID\n" +
                         "if you dont have the Apple IDyou can try to reset the phone")
    if "lost icloud" in text:
        bot.send_message(chat_id,
                         "")
    if "icloud" in text:
        bot.send_message(chat_id,
                         "If you want to create an icloud account type 'create icloud' or you lost your icloud type"
                         " 'lost icloud'")
    if "itunes" in text:
        bot.send_message(chat_id,
                         "You need ITUNES\n" +
                         "Connect your iphone to a computer with a usb cable if you have iTunes on your computer" +
                         " it will open itself and requires your iphone to be unlocked, allow iTunes to interact with "
                         "your device if it" +
                         " asks you. if you dont have iTunes download on the internet. When your device is connected to" +
                         "iTunes it will show an icon of an device just under Account, tap file and Add file to "
                         "library it will open a window which you will look for the song you want to add you can add "
                         "file or folder mark the song or folder and click open it will be added to iTunes then go to "
                         "the device icon and look for sync and tap sync it will be added to your IPhone")
    if "youtube videos" in text:
        bot.send_message(chat_id,
                         "First copy the link of the video you want on Youtube you do this tapping share button" +
                         "then tap copy link then go to AppStore download an app called DOCUMENTS READDLE once "
                         "finished " +
                         "open it and there is blue button after Add-ons drag that button to the left it will open a "
                         "browser " +
                         "then open a website called savefrom.net paste the youTube video link in the Enter url field "
                         "you " +
                         "will be given options to download once it is finished drag the blue folder from the bottom "
                         "left to " +
                         "the right you will find it in the folder downloads")
    if "scan documents" in text:
        bot.send_message(chat_id,
                         "Download an app from play store that scan documents and for iphone users use the note app" +
                         " tap the + button")
    if "battery draining" in text:
        bot.send_message(chat_id,
                         "If your phone is draining battery fast make sure there are no apps running in the background" +
                         "for iphone users click the home button twice to see apps running that are still open and "
                         "swipe" +
                         " up to close the app and for android user long press the home button to see the apps")

    # Routers
    if "router" in text:
        bot.send_message(chat_id,
                         "Type to any of the options to get "
                         "help\n\nChange router password\nBlock sites\nLog in")
    if "change router password" in text:
        bot.send_message(chat_id,
                         "Every router has a unique address like this 198.6.0.1 check at the back or bottom of your" +
                         "router to see the address if you cant find it search on the internet the ip address of "
                         "router's" +
                         "name now if you have seen it enter the ip address on the url of any browser and click search" +
                         "it will direct you to your router's site where you can change the name and password of your "
                         "wifi " +
                         "it will ask you to log in usually the username is admin check at back or bottom of your "
                         "router " +
                         "for username and password to log in once you log in go to WLAN and change the password and "
                         "or " +
                         "name of your wifi you can also hide your wifi from other people by clicking at hide "
                         "broadcast " +
                         "once you have changed your settings it will automatically log you out of the wifi network "
                         "and requires " +
                         "you to enter the wifi details you have changed and if you have hidden your network you will "
                         "not " +
                         "see it you have to add it manually by adding other network put the correct name and password " +
                         "then you will get connected")
    if "block sites" in text:
        bot.send_message(chat_id,
                         "Log in to the router address check the back of the router it will like this 198.8.0.1 " +
                         "you will also find the log in details at the back of your router once you logged in go to "
                         "Security" +
                         "tab and then choose URL Filter Config click new if you want to block you would place the "
                         "site url" +
                         " like this facebook.com or m.facebook.com and save")

    # Electronic transactions
    if "airtime" in text or "buddie" in text or "netone" in text or "telecel" in text:
        bot.send_message(chat_id,
                         "Buy any airtime or bills using ecocash,telecash,one wallet https://www.topup.co.zw")

    # Law and Rights
    if "citizen" in text or "rights" in text:
        bot.send_message(chat_id,
                         "DECLARATION OF RIGHTS: Chapter 4 of the constitution of Zimbabwe  www.zhrc.org.zw/your-rights" +
                         "\n\n\n1. Right to life.\n\n2. Right to personal liberty.\n\n3. Rights of arrests and "
                         "detained persons.\n\n" +
                         "4. Right to human dignity\n\n5. Right to personal security.\n\n6. Freedom from torture or "
                         "cruel, inhuman or" +
                         "degrading treatment or punishment\n\n7. Freedom from slavery and servitude\n\nFreedom from "
                         "forced labour.\n\n" +
                         "8. Equality and non-discrimination.\n\n9. Right to privacy\n\n10. Freedom of assembly and "
                         "association\n\nFreedom to demonstrate petition\n\n" +
                         "Freedom of conscience\n\nFreedom of expression and of media\n\nAccess to "
                         "information\n\nLanguage and culture\n\n" +
                         "Freedom of profession, trade or occupation\n\nLabour rights\n\nFreedom of movement and "
                         "residence\n\nPolitical rights\n\n" +
                         "Right to administrative justice\n\nRight to fair hearing\n\nRights of an accused "
                         "person\n\nProperty rights\n\n" +
                         "Rights to agricultural land\n\nEnvironmental rights\n\nFreedom from arbitrary "
                         "eviction\n\nRight to education\n\n" +
                         "Right to health care\n\nRight to food and water\n\nMarriage rights\n\nRights of "
                         "Children\n\nRights of Women\n\n" +
                         "Rights of the elderly\n\n Rights of persons with disabilities\n\nRights of liberation war "
                         "veterans")

    # Zim sites
    if "zimsec" in text or "commerce" in text or "maths" in text or "geography" in text or "shona" in text or "science" \
            in text or "account" in text:
        bot.send_message(chat_id,
                         "If you are looking for anything to do with Zimsec O Level and A Level notes, exam papers "
                         "and answers" +
                         " this the place to go.\n\nhttps://www.revision.co.zw/")
    if "rates" in text or "rtgs" in text or "bond" in text:
        bot.send_message(chat_id,
                         "Get all the latest rates in zimbabwe here. https://www.marketwatch.co.zw/")
    if "shop" in text and "online":
        bot.send_message(chat_id,
                         "Zimbabwe online store https://www.ownai.co.zw/")


while True:
    try:
        bot.polling()
    except Exception:
        time.sleep(15)
